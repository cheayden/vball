<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class UsersController extends AppController { 
    
    function login() {
        $this->set("cakeDescription", "Login");
    }
    
    function add() {
        if (!empty($this->data)) {
            if ($this->User->save($this->get_post_add_form())) {
                $this->Session->setFlash("User was successfully added");
                $this->redirect(array("action" => 'add'));
            }
        }
        $this->set("cakeDescription", "Register");
    }
    
    private function get_post_add_form(){
        return array(
                        "firstname" => $this->data["Users"]["firstname"],
                        "lastname" => $this->data["Users"]["lastname"],
                        "address" => $this->data["Users"]["address"],
                        "username" => $this->data["Users"]["username"],
                        "phone" => $this->data["Users"]["phone"],
                        "password" => SimplePasswordHasher::hash($this->data["Users"]["password"]),
                        "email" => $this->data["Users"]["email"],
                    );
    }
}